﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviour : MonoBehaviour {
	public float speed;
	private Vector2 axis;
	public Vector2 limits;
	public Propeller propeller;
	public ParticleSystem explosion;
	public GameObject graphics;
	private Collider2D myCollider;
	public AudioSource Shoot;
	public GameObject nau;
	public AudioSource Lives;
	void Awake(){
		myCollider = GetComponent<Collider2D>();
	}


	// Update is called once per frame
	void Update () {
		transform.Translate (axis * speed * Time.deltaTime);

		if (transform.position.x > limits.x) {
			transform.position = new Vector3 (limits.x, transform.position.y, transform.position.z);
		}else if (transform.position.x < -limits.x) {
			transform.position = new Vector3 (-limits.x, transform.position.y, transform.position.z);
		}

		if (transform.position.y > limits.y) {
			transform.position = new Vector3 (transform.position.x, limits.y, transform.position.z);
		}else if (transform.position.y < -limits.y) {
			transform.position = new Vector3 (transform.position.x, -limits.y, transform.position.z);
		}

		if (axis.y > 0) {
			propeller.RedFire ();
		} else if (axis.y < 0) {
			propeller.BlueFire ();
		} else {
			propeller.Stop ();
		}
	}

	public void SetAxis(Vector2 currentAxis){
		axis = currentAxis;
	}

	public void OnTriggerEnter2D(Collider2D other){
		if(other.tag == "Meteor"){
			Explode();
		}

		if (other.tag == "Lives") {
			Lives.Play();
			MyGameManager.getInsance ().sumavida ();
		}



	}

	private void Explode(){
		
		MyGameManager.getInsance ().loselive ();

		graphics.SetActive (false);
		//grafic
		myCollider.enabled = false;
		//parti
		explosion.Play();
		//audio
		Shoot.Play();

		//reset 2

		Invoke ("Hide",0.1f);
	
	}

	private void Hide(){
		nau.SetActive (false);
		Invoke ("Reset", 2);
}


	private void Reset(){
		nau.SetActive (true);
		graphics.SetActive (true);
		myCollider.enabled = true;
	}



}