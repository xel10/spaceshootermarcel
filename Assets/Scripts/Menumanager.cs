﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menumanager : MonoBehaviour {

    public void PlayGame()
    {

        SceneManager.LoadScene("SpaceShooter");
		Time.timeScale = 1f;

    }

    public void QuitGame()
    {

		Application.Quit();
    }


	public void Credits()
	{

		SceneManager.LoadScene("Credits");


	}
    
}
