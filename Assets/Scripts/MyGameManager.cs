﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MyGameManager : MonoBehaviour {
	public Text highscoretext;
	public Text livestext ;	
	public int lives;
	public int highscore;
	public GameObject pauseUI;

	public static bool GameIsPaused1 = false;
	public static bool GameIsPaused = false;
	// Use this for initialization

	private static MyGameManager instance;

	void Awake(){
		if (instance == null) {
			instance = this;
		}

	}
	public static MyGameManager getInsance (){
		return instance;
	}


	void Start () {
		lives = 3;
        highscore = 0;
        highscoretext.text = highscore.ToString("D5");
		// livestext.text = 
		livestext.text = "x "+ lives.ToString();
	}
	
	// Update is called once per frame
	void Update () {
		
//		if (lives == 0) {
//			if (GameIsPaused1) {
//				
//				Resume ();
//			} else {
//				
//				Pause ();
//				????ni puta  idea
//			}
//
//		}

		if (Input.GetKeyDown (KeyCode.Escape)) {
			if (GameIsPaused ) {
				Resume1 ();
			} else {
				Pause1 ();
			}
		}


		if (lives == 0) {
			SceneManager.LoadScene("GameOver");

		}


	}

	public void loselive(){
		lives--;
		livestext.text = "x " + lives.ToString ();

	}

	public void sumavida(){
		
			lives++;
			livestext.text = "x " + lives.ToString ();

	}


	public void sumapunts (){
        highscore += 10;
        highscoretext.text = highscore.ToString("D5");

    }




	public void ExitButton(){
		Application.Quit();
	}

	public void MenuButton(){
		SceneManager.LoadScene("MainMenu");
	}




	public void Resume1(){
		pauseUI.SetActive (false);
		Time.timeScale = 1f;
		GameIsPaused = false; 

	}

	void Pause1(){

		pauseUI.SetActive (true);
		Time.timeScale = 0f;
		GameIsPaused = true;


	}






}


