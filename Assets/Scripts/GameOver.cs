﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour {
	
	public static bool GameIsPaused = false;
	public GameObject gameoverUI;
	// Use this for initialization

	int lives = 0;

	// Update is called once per frame
	void Update () {
		
		if (lives == 0) {
			if (GameIsPaused) {
				Pause ();
			} else {
				
				Resume ();
			}
		}

	}
	void Resume(){
		gameoverUI.SetActive (false);
		Time.timeScale = 1f;
		GameIsPaused = false; 

	}

	void Pause(){

		gameoverUI.SetActive (true);
		Time.timeScale = 0f;
		GameIsPaused = true;


	}


	public void ExitButton(){
		Application.Quit();
	}

	public void MenuButton(){
		SceneManager.LoadScene("MainMenu");
	}
	public void PauseButton(){
		Resume ();
	}
}
