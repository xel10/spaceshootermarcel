﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpam : MonoBehaviour {


	public GameObject Enemy;

	float maxSpawnRateInSeconds = 5f;

	// Use this for initialization
	void Start () {
		Invoke ("EnemySpamer", maxSpawnRateInSeconds);

		InvokeRepeating ("IncreaseSpawnRate", 0f, 30f);
	}
	
	// Update is called once per frame
	void Update () {
		
	}


	void EnemySpamer(){


		//dal i baix de la pantalla
		Vector2 min = Camera.main.ViewportToWorldPoint (new Vector2 (0, 0));
		Vector2 max = Camera.main.ViewportToWorldPoint (new Vector2 (1, 1));





		GameObject anEnemy = (GameObject)Instantiate (Enemy);
		anEnemy.transform.position = new Vector2 (Random.Range (min.x, max.x), max.y);



		ScheduleNextEnemySpawn ();
	}

	void ScheduleNextEnemySpawn()
	{

		float spawnInNSeconds;

		if (maxSpawnRateInSeconds > 1f) 
		{

			spawnInNSeconds = Random.Range(1f, maxSpawnRateInSeconds);
		}
		else

			spawnInNSeconds = 1f;

			Invoke ("EnemySpamer", spawnInNSeconds);

		// per mes dificultat amb temps

	}

	void IncreaseSpawnRate(){

		if (maxSpawnRateInSeconds > 1f) {
			maxSpawnRateInSeconds--;
		}
		if (maxSpawnRateInSeconds == 1f) {
			CancelInvoke ("IncreaseSpawnRate");	


		}

	}
}
